# Chapter 4 Exercise

I should pick a system I need for my next project (which is now Escorts Survivor, a game jam bullet heaven game that answers the question, "Can't somebody else do it?") and try to model it with a system context diagram. Remember the three parts of such a diagram and that these diagrams are supposed to be nontechnical.

```mermaid
---
title: "Player Controller System in Godot: System Context Diagram"
---
flowchart TD
    User["Player
    [Person]
    
    A person I have coerced into playing my game"]

    C["Controller
    [Software System]
    
    The class responsible for passing commands\nfrom player input or AI to entities in-game"]

    PC["Player Character
    [Software System]
    
    The scene representing the in-game player character\nand its capabilities"]

    User-- "The player's inputs are interpreted by the Controller" -->C
    C-- "The Controller calls code in the Player Character to reflect player input in-game" -->PC

    classDef focusSystem fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff

    class User person
    class C,PC focusSystem
```