# Chapter 5

## Container Diagrams

The next stage of a C4 diagram is to make a container diagram. A container is a single deployable unit. Container diagrams still show no details about code but focus on interactions between containers rather than systems.

We'll define the first two containers in another example flowchart.

```mermaid
flowchart TD
    User["Premium Member
    [Person]
    A user of the website who has
    purchased a subscription"]

    WA["Web Application
    [.NET Core MVC Application]
    Allows memebers to view and review titles
    from a web browser. Also exposes
    an API for the mobile app"]

    MA["Mobile Application
    [Xamarin Application]
    Allows members to view and review
    titles from their mobile devices"]

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" --> WA

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" -->MA

    classDef container fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff

    class User person
    class WA,MA container
```

We can display the boundary of the system on container diagrams using subgraphs in a flowchart. Anything within that boundary belongs to the system we're architecting.

```mermaid
flowchart TD
    User["Premium Member
    [Person]

    A user of the website who has
    purchased a subscription"]

    WA["Web Application
    [.NET Core MVC Application]

    Allows memebers to view and review titles
    from a web browser. Also exposes
    an API for the mobile app"]

    MA["Mobile Application
    [Xamarin Application]

    Allows members to view and review
    titles from their mobile devices"]

    R[("In-Memory Cache
    [Redis]
    
    Titles and their reviews
    are cached")]

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" --> WA

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" -->MA

    subgraph listing-service[Listing Service]
    MA-- "Makes API calls to\n[HTTPS]" -->WA

    WA-- "Reads and writes to\n[REdis Serialization Protocol]" -->R
    end

    classDef container fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff

    class User person
    class WA,MA,R container

    style listing-service fill:none,stroke:#CCC,stroke-width:2px
    style listing-service color:#fff,stroke-dasharray:5 5
```

"Make sure you only put interactions in the subgraph that contains interactions where both nodes are part of the system you're modeling."

Now we can add supporting systems and show the interactions between our system's containers and its supporting systems.

```mermaid
flowchart TD
    User["Premium Member
    [Person]

    A user of the website who has
    purchased a subscription"]

    WA["Web Application
    [.NET Core MVC Application]

    Allows memebers to view and review titles
    from a web browser. Also exposes
    an API for the mobile app"]

    MA["Mobile Application
    [Xamarin Application]

    Allows members to view and review
    titles from their mobile devices"]

    R[("In-Memory Cache
    [Redis]
    
    Titles and their reviews
    are cached")]

    K["Message Broker
    [Kafka]
    
    Important domain events
    are published to Kafka"]

    TS["Title Service
    [Software System]
    
    Provides an API to retrieve
    title information"]

    RS["Reviews Service
    [Software System]
    
    Provides an API to retrieve
    and submit reviews"]

    SS["Search Service
    [Software System]
    
    Provides an API to search
    for titles"]

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" --> WA

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" -->MA

    subgraph listing-service[Listing Service]
    MA-- "Makes API calls to\n[HTTPS]" -->WA

    WA-- "Reads and writes to\n[REdis Serialization Protocol]" -->R
    end

    WA-- "Publishes messages to\n[Binary over TCP]" -->K
    WA-- "Makes API calls to\n[HTTPS]" -->TS
    WA-- "Makes API calls to\n[HTTPS]" -->RS
    WA-- "Makes API calls to\n[HTTPS]" -->SS

    classDef container fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff
    classDef supportingSystem fill:#666,stroke:#0b4884,color:#ffffff

    class User person
    class WA,MA,R container
    class TS,RS,SS,K supportingSystem

    style listing-service fill:none,stroke:#CCC,stroke-width:2px
    style listing-service color:#fff,stroke-dasharray:5 5
```

This diagram contains all the information we need, but it is hard to read because of the diagram's width. We can force Mermaid to render it in a more readable fashion using the nature of flowcharts. In flowcharts, every node is ranked. These ranks are automatically applied based on the definition and child nodes always sit below their parents.

```mermaid
flowchart TD
    User["Premium Member
    [Person]

    A user of the website who has
    purchased a subscription"]

    WA["Web Application
    [.NET Core MVC Application]

    Allows memebers to view and review titles
    from a web browser. Also exposes
    an API for the mobile app"]

    MA["Mobile Application
    [Xamarin Application]

    Allows members to view and review
    titles from their mobile devices"]

    R[("In-Memory Cache
    [Redis]
    
    Titles and their reviews
    are cached")]

    K["Message Broker
    [Kafka]
    
    Important domain events
    are published to Kafka"]

    TS["Title Service
    [Software System]
    
    Provides an API to retrieve
    title information"]

    RS["Reviews Service
    [Software System]
    
    Provides an API to retrieve
    and submit reviews"]

    SS["Search Service
    [Software System]
    
    Provides an API to search
    for titles"]

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" --> WA

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" -->MA

    subgraph listing-service[Listing Service]
    WA-- "Reads and writes to\n[REdis Serialization Protocol]" -->R

    MA
    end

    WA-- "Publishes messages to\n[Binary over TCP]" -->K
    WA-- "Makes API calls to\n[HTTPS]" -->TS
    WA-- "Makes API calls to\n[HTTPS]" -->RS
    WA-- "Makes API calls to\n[HTTPS]" -->SS

    classDef container fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff
    classDef supportingSystem fill:#666,stroke:#0b4884,color:#ffffff

    class User person
    class WA,MA,R container
    class TS,RS,SS,K supportingSystem

    style listing-service fill:none,stroke:#CCC,stroke-width:2px
    style listing-service color:#fff,stroke-dasharray:5 5
```

The person is still rank 1 but now the web application and mobile application sit side by side on rank 2.

```mermaid
flowchart TD
    User["Premium Member
    [Person]

    A user of the website who has
    purchased a subscription"]

    WA["Web Application
    [.NET Core MVC Application]

    Allows memebers to view and review titles
    from a web browser. Also exposes
    an API for the mobile app"]

    MA["Mobile Application
    [Xamarin Application]

    Allows members to view and review
    titles from their mobile devices"]

    R[("In-Memory Cache
    [Redis]
    
    Titles and their reviews
    are cached")]

    K["Message Broker
    [Kafka]
    
    Important domain events
    are published to Kafka"]

    TS["Title Service
    [Software System]
    
    Provides an API to retrieve
    title information"]

    RS["Reviews Service
    [Software System]
    
    Provides an API to retrieve
    and submit reviews"]

    SS["Search Service
    [Software System]
    
    Provides an API to search
    for titles"]

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" --> WA

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" -->MA

    subgraph listing-service[Listing Service]
    MA-- "Makes API calls to\n[HTTPS]" -->WA

    WA-- "Reads and writes to\n[REdis Serialization Protocol]" -->R
    end

    WA-- "Publishes messages to\n[Binary over TCP]" --->K
    WA-- "Makes API calls to\n[HTTPS]" --->TS
    WA-- "Makes API calls to\n[HTTPS]" --->RS
    WA-- "Makes API calls to\n[HTTPS]" --->SS

    classDef container fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff
    classDef supportingSystem fill:#666,stroke:#0b4884,color:#ffffff

    class User person
    class WA,MA,R container
    class TS,RS,SS,K supportingSystem

    style listing-service fill:none,stroke:#CCC,stroke-width:2px
    style listing-service color:#fff,stroke-dasharray:5 5
```

By adding extra hyphens to the arrowhead before child nodes, we ask Mermaid to increase the node's rank. This doesn't always work.

Now we will add dotted lines to represent an asynchronous relationship.

```mermaid
flowchart TD
    User["Premium Member
    [Person]

    A user of the website who has
    purchased a subscription"]

    WA["Web Application
    [.NET Core MVC Application]

    Allows memebers to view and review titles
    from a web browser. Also exposes
    an API for the mobile app"]

    MA["Mobile Application
    [Xamarin Application]

    Allows members to view and review
    titles from their mobile devices"]

    R[("In-Memory Cache
    [Redis]
    
    Titles and their reviews
    are cached")]

    K["Message Broker
    [Kafka]
    
    Important domain events
    are published to Kafka"]

    TS["Title Service
    [Software System]
    
    Provides an API to retrieve
    title information"]

    RS["Reviews Service
    [Software System]
    
    Provides an API to retrieve
    and submit reviews"]

    SS["Search Service
    [Software System]
    
    Provides an API to search
    for titles"]

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" --> WA

    User-- "Views titles, searches titles
    and reviews titles using
    [HTTPS]" -->MA

    subgraph listing-service[Listing Service]
    MA-- "Makes API calls to\n[HTTPS]" -->WA

    WA-- "Reads and writes to\n[REdis Serialization Protocol]" -->R
    end

    WA-. "Publishes messages to\n[Binary over TCP]" ..->K
    WA-- "Makes API calls to\n[HTTPS]" --->TS
    WA-- "Makes API calls to\n[HTTPS]" --->RS
    WA-- "Makes API calls to\n[HTTPS]" --->SS

    classDef container fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff
    classDef supportingSystem fill:#666,stroke:#0b4884,color:#ffffff

    class User person
    class WA,MA,R container
    class TS,RS,SS,K supportingSystem

    style listing-service fill:none,stroke:#CCC,stroke-width:2px
    style listing-service color:#fff,stroke-dasharray:5 5
```

This is done by replacing some hyphens with dots in the syntax. Extra dots are used instead of extra hyphens to promote the child node's rank.

```
WA-. "Publishes messages to\n[Binary over TCP]" ..->K
```

Check before the end of Chapter 5 for a list of the syntax for all the kinds of arrows you can use in flowcharts.