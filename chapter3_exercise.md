# Chapter 3 Exercise

I am to model the application flow of my project with a sequence diagram. This is a little unwieldy to me as I am new to software architecture, so I'll try to make a simple one (even though the exercise explicitly asks me to try a complex one.)

```mermaid
---
title: Game Logic
---
sequenceDiagram
autonumber
    actor Player
    participant Game
    participant init as _init
    participant ready as _ready
    participant physics_process as _physics_process
    participant process as _process

    Player ->> Game: Start game
    activate Game
    Game ->> init: Do everything all nodes in scene require before ready
    activate init
    init ->> ready: Pass off to ready
    deactivate init
    Game ->> ready: Do everything all nodes in scene require on ready
    activate ready
    ready ->> physics_process: Pass off to _physics_process
    ready ->> process: Pass off to _process
    deactivate ready
    activate physics_process
    physics_process ->> physics_process: Run for every physics tick a second
    activate process
    process ->> process: Run as many times a second as possible
    Player ->> Game: End game
    deactivate physics_process
    deactivate process
    deactivate Game

```

This probably isn't a very accurate model, but it applies what I've learned so I'm sticking with it.