# Chapter 3

Sequence diagrams can be used to model how systems interact, particularly at a high-level. All sequence diagrams have actors and participants. "An actor represents a human, and a participant represents a process (for example, a service or database that runs in a UNIX process.)"

Account for actors and participants by starting sequence diagrams as in the following example. Note that unlike class diagrams, sequence diagrams allow spaces in node identifiers.

```mermaid
---
title: User Sign Up Flow
---
sequenceDiagram
    actor Browser
    participant Sign Up Service
    participant User Service
    participant Kafka
```

Nodes in a sequence are called lifelines in UML. Defining actors and participants as optional, but marking nodes as actors is the only way to draw them as stick figures as seen (and more documentation is a good thing.) Defining actors and participants also allows you to change the order they are drawn in the model by changing the order they are defined.

You can define aliases for participants as in the following markup block. You can then use the shorter alias when defining messages between participants.

```
participant SUS as Sign Up Service
```

## Adding our first interaction

The diagram below shows an example of modeling interactions in a sequence diagram.

```mermaid
---
title: User Sign Up Flow
---
sequenceDiagram
    actor Browser
    participant SUS as Sign Up Service
    participant US as User Service
    participant Kafka

    Browser ->> SUS: GET /sign_up
    SUS -->> Browser: 200 OK (HTML page)
```

"In sequence diagrams, interactions between nodes are modeled in the form of messages." In the model above, there are two messages; the first one from the browser to the sign up service to request the page and the response from the service containing the HTML for the browser to render.

"Messages can be added in a variety of formats, and we'll cover more later, but for nwo we want to use ->> for synchronous calls, which will show a solid line with a solid arrowhead. For reply messages, we use -->> which will show a dotted line with a solid arrowhead. This allows the reader to easily distinguish between requests and responses.

The node sending the message goes on the left of the arrow, and the node receiving the messages goes on the right.

Finally, each message should be labeled with a brief of description."

## Branching logic

Most flows will have at least a happy path (where everything goes perfectly) and an unhappy path (there are usually several.) Don't detail everything that could go wrong for every unhappy path, but instead detail a few major elements of an unhappy path in order to identify the major area where error handling should be focused. Create separate sequence diagrams to model more unhappy paths to keep each model readable.

Below is an example of detailing one unhappy path as well as the happy path.

```mermaid
---
title: User Sign Up Flow
---
sequenceDiagram
    actor Browser
    participant SUS as Sign Up Service
    participant US as User Service
    participant Kafka

    Browser ->> SUS: GET /sign_up
    SUS -->> Browser: 200 OK (HTML page)

    Browser ->> SUS: POST /sign_up
    SUS ->> SUS: Validate input

    alt invalid input
    SUS -->> Browser: Error
    else valid input
    SUS ->> US: POST /users
    US -->> SUS: 201 Created (User)
    SUS -->> Browser: 301 Redirect (Login page)
    end
```

The following is the markup block which defines the "alt" box where the branching logic in the model above is. They appear just like conditional statements from programming languages.

```
alt invalid input
SUS -->> Browser: Error
else valid input
SUS ->> US: POST /users
US -->> SUS: 201 Created (User)
SUS -->> Browser: 301 Redirect (Login page)
end
```

You define multiple else statements to handle more than two branches as seen in the example markup block below. Keeping long series of else statements can have an impact on readability, however.

```
alt first case
A ->> B: first case
else second case
A ->> C: second case
else third case
A ->> D: third case
end
```

## Asynchronous Messages

So far we have only covered synchronous messaging. Asynchronous messaging is being leveraged more and more, however, and it can be modeled in Mermaid as in the example below.

```mermaid
---
title: User Sign Up Flow
---
sequenceDiagram
    actor Browser
    participant SUS as Sign Up Service
    participant US as User Service
    participant Kafka

    Browser ->> SUS: GET /sign_up
    SUS -->> Browser: 200 OK (HTML page)

    Browser ->> SUS: POST /sign_up
    SUS ->> SUS: Validate input

    alt invalid input
    SUS -->> Browser: Error
    else valid input
    SUS ->> US: POST /users
    US --) Kafka: User Created Event Published
    US -->> SUS: 201 Created (User)
    SUS -->> Browser: 301 Redirect (Login page)
    end
```

We added one asynchronous message to the model above using the syntax ```--)```.

## Activations

Activations can be used to show when requests begin and when the process ends with rectangular boxes. See the model below.

```mermaid
---
title: User Sign Up Flow
---
sequenceDiagram
    actor Browser
    participant SUS as Sign Up Service
    participant US as User Service
    participant Kafka

    Browser ->> SUS: GET /sign_up
    activate SUS
    SUS -->> Browser: 200 OK (HTML page)
    deactivate SUS

    Browser ->>+ SUS: POST /sign_up
    SUS ->> SUS: Validate input

    alt invalid input
    SUS -->> Browser: Error
    else valid input
    SUS ->>+ US: POST /users
    US --) Kafka: User Created Event Published
    US -->>- SUS: 201 Created (User)
    SUS -->>- Browser: 301 Redirect (Login page)
    end
```

The below markup block demonstrates how the first activation is drawn above. The rest are drawn by adding + and - to either end of relationship syntax to mark activation and deactivation, respectively, on either entity.

```
Browser ->> SUS: GET /sign_up
activate SUS
SUS -->> Browser: 200 OK (HTML page)
deactivate SUS
```

## Notes

"Labels should be kept small, but if you need to add additional context or want to highlight a particular part of the sequence, notes are what you should use."

The final version of the sequence diagram example model, with a note, is modelled below.

```mermaid
---
title: User Sign Up Flow
---
sequenceDiagram
    actor Browser
    participant SUS as Sign Up Service
    participant US as User Service
    participant Kafka

    Browser ->> SUS: GET /sign_up
    activate SUS
    SUS -->> Browser: 200 OK (HTML page)
    deactivate SUS

    Browser ->>+ SUS: POST /sign_up
    SUS ->> SUS: Validate input

    alt invalid input
    SUS -->> Browser: Error
    else valid input
    SUS ->>+ US: POST /users
    US --) Kafka: User Created Event Published
    Note left of Kafka: other services take action based on this event
    US -->>- SUS: 201 Created (User)
    SUS -->>- Browser: 301 Redirect (Login page)
    end
```

And here is the syntax used to place that note:

```
Note left of Kafka: other services take action based on this event
```

Replace left with right with obvious effects and replace Kafka with the name of the node you are placing a note on. Notes can also span over multiple nodes with syntax like that of the example markup block below.

```
Note over US, Kafka: other services take action based on this event
```

## Annotating with sequence numbers

Mermaid can provide automatic sequence numbering. This adds a number to each message on the diagram. This is done by simply adding ```autonumber``` below the ```sequenceDiagram``` line. Below is the first section of our model with autonumbering.

```mermaid
---
title: User Sign Up Flow
---
sequenceDiagram
autonumber
    actor Browser
    participant SUS as Sign Up Service
    participant US as User Service
    participant Kafka

    Browser ->> SUS: GET /sign_up
    activate SUS
    SUS -->> Browser: 200 OK (HTML page)
    deactivate SUS

    Browser ->>+ SUS: POST /sign_up
    SUS ->> SUS: Validate input
```

## Dropdown menus

You can add dropdown menus to actors and participants in a sequence diagram with a JSON-like format using key-value pairs as seen in the markup block below.

```
links US: {"Repository": "https://www.example.com/repository"}
```

You can add as many key-value pairs as you like.