# Chapter 4

## C4 Diagrams

The C4 model was created as a consistent mechanism to model software architecture while minimizing the gap between software architecture models and source code.

A C4 diagram has four parts:

- System context
- Container
- Component
- Code

Moving through each part of the diagram reveals more and more technical details. C4 diagrams have no defined notation, so feel free to experiment while keeping notation consistent.

## System Context Diagram

System context diagrams are the highest level view of software architecture and should be nontechnical. There are three main elements in a system context diagram:

- People
- The software system you are designing
- Supporting software systems

We'll model our example diagrams using a flowchart.

```mermaid
flowchart TD
    User["Premium Member
    [Person]

    A user of the website who has
    purchased a subscription"]
```

As usual, the first line defines the type of diagram, in this case a top-down flowchart.

```
flowchart TD
```

TD is an optional parameter to define the direction of the flowchart. This parameter accepts the following options:

- TB: top-to-bottom
- TD: top-down (same as top-to-bottom)
- BT: bottom-to-top
- RL: right-to-left
- LR: left-to-right

After the first line, a node in the flowchart is defined. It is only defined once and then used by id later on. In the above model "User" is the id. The syntax for defining nodes is below.

```
format id["Label / Description"]
```

Label and Description refers to the text that goes in the node that's rendered. "Following Simon Brown's notation, each node should contain a title, label, and description. The title should clearly outline the node, the label is what type the node is, and the description briefly describes what that node represents. At this level, the label is perhaps superfluous, but in more detailed views that follow, it's essential, and I like to keep the layout consistent between the different levels of the C4 model."

After defining the first node, we'll add our system to the model.

```mermaid
flowchart TD
    User["Premium Member
    [Person]
    
    A user of the website who has
    purchased a subscription"]

    LS["Listings Service
    [Software System]

    Serves web pages displaying title
    listings to the end user"]
```

We can add arrowheads to show dependencies in system context diagrams.

```mermaid
flowchart TD
    User["Premium Member
    [Person]
    
    A user of the website who has\npurchased a subscription"]

    LS["Listings Service
    [Software System]

    Serves web pages displaying title\nlistings to the end user"]

    User-- "Views titles, searches titles\nand reviews titles using" -->LS
```

The last line of the model markup draws the connected nodes with this syntax.

```
User-- "Views titles, searches titles\nand reviews titles using" -->LS
```

We model the arrows as dependencies and briefly describe what the parent node relies on from the child node.

All that remains to be added to the system context diagram are supporting systems. "These are any systems that your system interacts with and that are required for it to do its job."

```mermaid
flowchart TD
    User["Premium Member
    [Person]
    
    A user of the website who has\npurchased a subscription"]

    LS["Listings Service
    [Software System]

    Serves web pages displaying title\nlistings to the end user"]

    TS["Title Service
    [Software System]

    Provides an API to retrieve\ntitle information"]

    RS["Review Service
    [Software System]
    
    Provides an API to retrieve\nand submit reviews"]

    SS["Search Service
    [Software System]
    
    Provides an API to search\nfor titles"]

    User-- "Views titles, searches titles\nand reviews titles using" -->LS

    LS-- "Retrieves title information from" -->TS
    LS-- "Retrieves from and submits reviews to" -->RS
    LS-- "Searches for titles using" -->SS
```

Nodes in C4 models can be styled using a combination of CSS and SVG styling options.

```mermaid
---
title: "Listing Service C4 Model: System Context"
---
flowchart TD
    User["Premium Member
    [Person]
    
    A user of the website who has\npurchased a subscription"]

    LS["Listings Service
    [Software System]

    Serves web pages displaying title\nlistings to the end user"]

    TS["Title Service
    [Software System]

    Provides an API to retrieve\ntitle information"]

    RS["Review Service
    [Software System]
    
    Provides an API to retrieve\nand submit reviews"]

    SS["Search Service
    [Software System]
    
    Provides an API to search\nfor titles"]

    User-- "Views titles, searches titles\nand reviews titles using" -->LS

    LS-- "Retrieves title information from" -->TS
    LS-- "Retrieves from and submits reviews to" -->RS
    LS-- "Searches for titles using" -->SS

    classDef focusSystem fill:#1168bd,stroke:#0b4884,color:#ffffff
    classDef supportingSystem fill:#666,stroke:#0b4884,color:#ffffff
    classDef person fill:#08427b,stroke:#052e56,color:#ffffff

    class User person
    class LS focusSystem
    class TS,RS,SS supportingSystem
```

First you define classes using the syntax ```classDef className styleProperties```. Then you can use that style on any nodes by attaching that class using the syntax ```class nodeID(s) className```.

Generally, you should put all styling at the end to separate form from function and keep the markup readable.