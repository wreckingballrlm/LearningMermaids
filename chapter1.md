# Chapter 1

In a Mermaid diagram, the first line defines the type of relationship. For now we are modeling class diagrams.

Every line after the first defines relationships between entities and therefore how to draw the diagram. Each relationship is defined by two entities with a set of symbols between them determining how their relationship is drawn. There are three main types of relationships defined below.

## Associations

```mermaid
classDiagram
    Title -- Genre
```

In UML, the loosest form of relationship is an **association**. For a relationship to be classed as an association the entities must be able to exist independently and usually have their own life cycles. Also, there is generally no owner of the relationship; the entities are merely linked and use one another rather than one owning the other.

The syntax for an association is a pair of hyphens (--) between two entities.

## Compositions

```mermaid
classDiagram
    Title -- Genre
    Title *-- Season
    Title *-- Review

    Season *-- Episode
```

Entities in models often don't make sense on their own. In order to model slightly more complex relationships where entities require the context of which entities they belong to, one can use a type of relationship called a **composition**.

The syntax for a composition is a pair of hyphens with an asterisk on one side (\*--, --\*) between two entities, with the asterisk determining which entity holds the reference, or in other words, which entity is the parent.

(The book I'm reading[^1] recommends always putting the parent on the left for "easier readability and less cognitive load of working out the direction of the relationship." That makes sense to me. The book also recommends grouping each entity's relationships together with empty lines separating the groups.)

## Aggregates

```mermaid
classDiagram
    Title -- Genre
    Title *-- Season
    Title *-- Review
    Title o-- Actor

    Season *-- Episode
```

In an aggregate relationship, there is still a parent and a child, but their relationship is looser and the child could still exist without the parent. Aggregates are between associations and compositions in terms of how closely the entities are grouped together.

The syntax for an aggregate relationship is a pair of hyphens with an o on one side (o--, --o) between two entities.

## When to use each relationship

The book offers the following guidance on when to use each of the three main relationships between entities.

- **Associations**: "There's a relationship between the entities, with at least one entity holding a reference to the other. There's no owner of the relationship though, and they can exist completely independently of one another." An example is a *teacher* and *student* relationship.
- **Aggregations**: "There's a more direct relationship between the entities than an association, but they can still exist independently of one another. There's an owner of the relationship, but if the parent is deleted, the child can still remain." A *teacher* has an aggregate relationship to a *class*. If you delete the teacher, the class still exists and makes sense on its own.
- **Compositions**: "The closest of relationships is reserved for compositions. Similarly to aggregations, there's an owner of the relationship. However, if the parent is deleted, the child must be deleted, too, and makes no sense without its parent relationship." A *grade* could be an entity with a compositon relationship to a *class*. If class is deleted, grade most likely wouldn't make sense.

[^1]: The book I'm reading is *Creating Software with Modern Diagramming Techniques* by Ashley Peacock.
