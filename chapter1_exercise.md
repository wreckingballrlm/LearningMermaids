# Chapter 1 Exercise

For my first exercise, I must create my own domain model. I will be using this model throughout the book to apply what I learn. I will be modeling my next project in Godot[^1].

## Working from vague to specific

I will start by modeling how projects in Godot work in general. Godot uses a tree structure made of scenes and nodes. Each scene is made of nodes, with a root node, any number of child nodes (branches or leaves), each with any number of child nodes. A scene can be instanced in another scene as a singular node, either hiding its complete nature or allowing you to edit the instance's children.

Godot has many types of nodes, almost all of which require a relationship to at least one other node to make sense. There are also plenty of nodes which can exist independently, albeit perhaps not very usefully. Therefore, nodes can generally form any kind of relationship with each other.

Projects in Godot also contain resources such as art assets, file paths, custom resources, and many more. Nodes hold references to this information. Nodes also have many properties which can be adjusted either in the inspector or in code.

```mermaid
classDiagram
    Project *-- Scenes
    Project *-- ProjectSettings

    Scenes *-- Nodes
    
    Nodes -- Nodes
    Nodes *-- Nodes
    Nodes o-- Nodes
    Nodes *-- Properties
    Nodes *-- Resources
    Nodes *-- Scripts
```

There is one kind of relationship in Godot which I will use in nearly every (2D) project I work on. It is modeled below.

```mermaid
classDiagram
    CharacterBody2D *-- Script
    CharacterBody2D *-- CollisionShape2D
    CharacterBody2D *-- Sprite2D
    CharacterBody2D *-- AnimationPlayer
    CharacterBody2D *-- Camera2D

    CollisionShape2D *-- Shape

    Sprite2D -- AnimationPlayer
    Sprite2D *-- Texture

    AnimationPlayer *-- Animations
```

This model represents a common set up for player scenes in Godot.

## My next project

I am starting a new project in Godot very soon. I will do my best to model it piece by piece in the exercises from the book, then compile the models in the project's repo[^2].

I will start by using the first pattern I've identified in Godot, the Player. I don't know much about this new project yet so it will be very barebones, but as I move on with the book as well as the project itself, I will become much more capable of filling out this domain model.

(A name followed by "GD" refers to GDScript files containing code. They are almost always attached to the root node of a scene and named after the root node. Their file extension is .gd. More generally, names in lowercase and snake_case refer to filenames, either scripts or resources, and their file extension will follow in all caps.)

```mermaid
classDiagram
    Player *-- playerGD
    Player *-- CollisionShape2D
    Player *-- Sprite2D
    Player *-- AnimationPlayer

    CollisionShape2D *-- RectangleShape

    Sprite2D *-- player_spritePNG

    AnimationPlayer -- Sprite2D
    AnimationPlayer *-- idle
    AnimationPlayer *-- move
    AnimationPlayer *-- attack
```

In Godot's file system, there is also a very simple to model hierarchy for the Player scene. First, the general structure.

```mermaid
classDiagram
    Folder *-- Folder
    Folder *-- Scenes
    Folder *-- Resources
```

Now the model for this particular scene, the Player.

```mermaid
classDiagram
    player *-- playerTSCN
    player *-- playerGD
```

This is pretty much all I can model for this project at its outset. I accomplished my main goal, which was to practice using the three main relationships in UML while drawing Mermaid diagrams.

[^1]: Godot is a free, open-source game engine.

[^2]: https://github.com/wreckingballrlm/ForebodingDungeon