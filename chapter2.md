# Chapter 2

```mermaid
classDiagram
    Title -- Genre
    Title *-- Season
    Title *-- Review
    Title o-- Actor

    TVShow --|> Title
    Short --|> Title
    Film --|> Title

    Season *-- Episode
```

## Generalizations

**Generalizations** are relationships which behave exactly the same way as inheritance in programming. In the example model, there are three nodes which inherit from Title and point to it.

The syntax for generalizations is an arrow (--|>) pointing from the entity which is inheriting to the entity it is inheriting from.

## Rendering in Mermaid

Mermaid's rendering system allows you to create and update complex diagrams by changing a few lines of markup. Try commenting out lines in the diagram below and viewing the preview to see how Mermaid re-renders the diagram. (Comments in Mermaid are made using %% at the start of the line.)

```mermaid
classDiagram
    Title -- Genre
    Title *-- Season
    Title *-- Review
    Title o-- Actor

    TVShow --|> Title
    Short --|> Title
    Film --|> Title

    Season *-- Review
    Season *-- Episode

    Episode *-- Review
```

## Describe relationships in Mermaid

Relationships in Mermaid can be described in-model and often should be. The diagram below shows descriptions for each of the kinds of relationship we've learned about so far. Descriptions are added by adding a colon to the end of the line and then writing your description.

It also shows **directional association** from the Viewer node to the Title node. Directional associations are relationships in which two entities are loosely related and one entity holds a reference to the other but not vice versa.

The syntax for directional associations is to add an arrowhead to one end of an association (<--, -->) between two entities. The simpler form of associations are **bidirectional associations**.

```mermaid
classDiagram
    Title -- Genre: is associated with
    Title *-- Season: has
    Title *-- Review: has
    Title o-- Actor: features

    TVShow --|> Title: implements
    Short --|> Title: implements
    Film --|> Title: implements

    Viewer --> Title: watches

    Season *-- Review: has
    Season *-- Episode: contains

    Episode *-- Review: has
```

Bidirectional relationships should be described from a point of view befitting either entity. For all other relationships, they should be described from the point of view of the parent.

## Multiplicity

Models in Mermaid can employ **multiplicity** to describe the units involved in a relationship. It "allows you to define whether relationships are one-to-one, one-to-many, none-to-many, many-to-many, and everything in between."

```mermaid
classDiagram
    Title "1..*" -- "1..*" Genre: is associated with

    Title "1" *-- "0..*" Season: has
    Title "1" *-- "0..*" Review: has
    Title "0..*" o-- "1..*" Actor: features

    TVShow --|> Title: implements
    Short --|> Title: implements
    Film --|> Title: implements

    Viewer "0..*" --> "0..*" Title: watches

    Season "1" *-- "0..*" Review: has
    Season "1" *-- "1..*" Episode: contains

    Episode "1" *-- "0..*" Review: has
```

The syntax for multiplicity is to put a number or range of numbers in quotation marks on between the entity and the relationship on either side ("1", "0..\*", "1..\*", and so on.)

Multiplicity is defined on either side of the relationship, between an entity and the relationship. "Each side is known as a single entity's cardinality. ... An entity's cardinality is defined on the opposite side of the relationship."

The book has the following advice on remembering how cardinality is defined: "To help remember how cardinality is defined, particularly when I am looking at past diagrams, I find it helps to mentally "skip" the first cardinality you get to."

## Titles

Diagrams can be labeled with titles very easily in Mermaid. Simply add a block of code, as seen below, to the very top of a block of Mermaid markup, before the diagram type definition.

```
---
title: Title Name
---
classDiagram
```

## Readability in Mermaid

For the most part, you cannot change the way Mermaid renders your diagrams. One way to alter it slightly is to affect the height of certain boxes in a class diagram as in the following block of markup. Add the line seen anywhere in the block of Mermaid markup after the first time the NodeName appears to add two empty boxes to the node.

```
NodeName: \n\n
```

See this trick in action with this new version of our diagram. We have also added a title to the diagram.

```mermaid
---
title: Streamy
---
classDiagram
    Title "1..*" -- "1..*" Genre: is associated with

    Title "1" *-- "0..*" Season: has
    Title "1" *-- "0..*" Review: has
    Title "0..*" o-- "1..*" Actor: features

    TVShow --|> Title: implements
    Short --|> Title: implements
    Film --|> Title: implements

    Viewer "0..*" --> "0..*" Title: watches

    Season "1" *-- "0..*" Review: has
    Season "1" *-- "1..*" Episode: contains

    Episode "1" *-- "0..*" Review: has

    %% Added to enhance readability
    Title: \n\n
```

## Adding links to nodes

Each node in a diagram can have a link related to it that makes it clickable. This can be used to link to outside documentation or anything else. Add links to nodes using the following syntax anywhere after NodeName is used.

```
link NodeName "https://www.example.com" _blank
```

The link keyword tells Mermaid we want to create a link, NodeName is the identifier of the node, followed by a URL, and then the setting for how the link is opened.